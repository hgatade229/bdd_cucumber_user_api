Feature: Trigger the User_Update API and validate response parametres 

@update_user
Scenario: Trigger the User_Update API request with valid request body parametres
           Given lastName  in request body for Update API
           When Send the User_Update request with payload to the Endpoint
           Then validate status code for Update API
           And validate response body parametres for Update API 
            