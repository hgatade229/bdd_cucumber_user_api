Feature: Configure and Trigger the Get API and store Response 

@get_all_user
Scenario: Trigger the Get API with Configuration
           Given Configure Get API for All Users	
           When Send the Get request with required Endpoint
           Then validate status code for Get API
            