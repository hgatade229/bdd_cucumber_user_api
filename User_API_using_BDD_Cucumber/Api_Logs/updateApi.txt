Endpoint is :
https://dummyjson.com/users/1

Request Body is :
{
    "lastName": "Gatade"
}

Executed on Date is :
Sun, 17 Mar 2024 15:45:38 GMT

Response Body  is :
{"id":1,"firstName":"Terry","lastName":"Gatade","maidenName":"Smitham","age":50,"gender":"male","email":"atuny0@sohu.com","phone":"+63 791 675 8914","username":"atuny0","password":"9uQFF1Lh","birthDate":"2000-12-25","image":"https://robohash.org/Terry.png?set=set4","bloodGroup":"A-","height":189,"weight":75.4,"eyeColor":"Green","hair":{"color":"","type":""},"domain":"slashdot.org","ip":"117.29.86.254","address":{"address":"","city":"","coordinates":{"lat":null,"lng":null},"postalCode":"","state":""},"macAddress":"13:69:BA:56:A3:74","university":"Capitol University","bank":{"cardExpire":"","cardNumber":"","cardType":"","currency":"","iban":""},"company":{"address":{"address":"","city":"","coordinates":{"lat":null,"lng":null},"postalCode":"","state":""},"department":"","name":"","title":""},"ein":"20-9487066","ssn":"661-64-2976","userAgent":"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.24 (KHTML, like Gecko) Chrome/12.0.702.0 Safari/534.24"}

