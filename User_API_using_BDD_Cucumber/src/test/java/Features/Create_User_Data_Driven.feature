Feature: Trigger Create API on the basis of Insert Data

@data_driven
Scenario Outline: Trigger the Create API request with valid request parametres
                  Given "<lastName>" and "<firstName>" and <age> in request body	
                  When Send the request with payload to the Endpoint
                  Then validate status code
                  And validate response body parametres                   
Examples: 

          |firstName | lastName| age|
          |Mayur| Singh| 25|                   