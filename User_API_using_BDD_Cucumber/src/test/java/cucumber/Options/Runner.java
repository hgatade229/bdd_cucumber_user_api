package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/Features", glue = "StepDefinations" , tags = "@create_user or @data_driven or @delete_api or @get_all_user or @update_user ")
public class Runner {

}
