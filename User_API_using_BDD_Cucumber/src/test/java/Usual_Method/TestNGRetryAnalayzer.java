package Usual_Method;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class TestNGRetryAnalayzer implements IRetryAnalyzer{

	
	private int countstart = 0;
	private int countend = 4;

	@Override
	public boolean retry(ITestResult result) {
		// TODO Auto-generated method stub
		if (countstart < countend) {
			String testcasename = result.getName();
			System.out.println(testcasename + " failed in current iteration " + countstart + ", so retrying for "
					+ (countstart + 1));
			countstart ++;
			return true;
		}
		return false;
	}

}
