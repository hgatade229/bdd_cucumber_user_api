package Usual_Method;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Api_Trigger {

	public static Response CreateUserApi(String headername,String headervalue,String req_Body,String endpoint)
	{
		
		RequestSpecification req_spec = RestAssured.given();
		
		req_spec.header(headername , headervalue);
		
		req_spec.body(req_Body);
		
		Response response = req_spec.post(endpoint);
		
		return response;
		
	}
	
	public static Response UpdateUserApi(String headername,String headervalue,String req_Body,String endpoint)
	{
		
		RequestSpecification req_spec = RestAssured.given();
		
		req_spec.header(headername , headervalue);
		
		req_spec.body(req_Body);
		
		Response response = req_spec.put(endpoint);
		
		return response;
		
	}
	
	
	public static Response GetUserApi(String headername,String headervalue,String endpoint)
	{
		
		RequestSpecification req_spec = RestAssured.given();
		
		req_spec.header(headername , headervalue);
		
		Response response = req_spec.get(endpoint);
		
		return response;
		
	}
	
	
	
	public static Response DeleteUserApi(String headername,String headervalue,String endpoint)
	{
		
		RequestSpecification req_spec = RestAssured.given();
		
		req_spec.header(headername , headervalue);
		
		Response response = req_spec.delete(endpoint);
		
		return response;
		
	}
}