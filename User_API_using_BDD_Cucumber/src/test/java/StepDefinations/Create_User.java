package StepDefinations;

import java.io.File;
import java.io.IOException;

import Usual_Method.Api_Trigger;
import Usual_Method.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import junit.framework.Assert;
import repository.RequestBody;

public class Create_User {

	String requestBody;

	Response response;

	int statuscode;


    @Given("{string} and {string} and {int} in request body")
    public void and_and_in_request_body(String firstname, String lastname,int age) throws IOException {
	
		File dir_name = Utility.CreateLogDir("Api_Logs");

	    requestBody = "{\r\n" + "    \"firstName\": \""+firstname+"\",\r\n" + "    \"lastName\": \""+lastname+"\",\r\n"+"  \"age\":\""+age+"\"\r\n" + "}";
	
		String endpoint = RequestBody.Hostname() + RequestBody.CreateUserResource();

		response = Api_Trigger.CreateUserApi(RequestBody.Headername(), RequestBody.Headervalue(), requestBody,
				endpoint);
		
		Utility.CreateEvidanceFile("createApiInputData", dir_name, endpoint, requestBody, response.getHeader("Date"),
				response.getBody().asString());

      } 

	@Given("firstName , lastName and age in request body")
	public void first_name_last_name_and_age_in_request_body() throws IOException {

		File dir_name = Utility.CreateLogDir("Api_Logs");

		requestBody = RequestBody.createBody("create_tc2");

		String endpoint = RequestBody.Hostname() + RequestBody.CreateUserResource();

		response = Api_Trigger.CreateUserApi(RequestBody.Headername(), RequestBody.Headervalue(), requestBody,
				endpoint);

		Utility.CreateEvidanceFile("createApi", dir_name, endpoint, requestBody, response.getHeader("Date"),
				response.getBody().asString());

	}

	@When("Send the request with payload to the Endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {

		statuscode = response.statusCode();

		String responsebody = response.asPrettyString();

		System.out.println(statuscode);

		//System.out.println(responsebody);

	}

	@Then("validate status code")
	public void validate_status_code() {

		Assert.assertEquals(response.statusCode(), 200);

	}

	@Then("validate response body parametres")
	public void validate_response_body_parametres() {

		JsonPath j_req = new JsonPath(requestBody);


		Assert.assertEquals(j_req.getString("firstName"), response.jsonPath().getString("firstName"));
		Assert.assertEquals(j_req.getString("lastName"), response.jsonPath().getString("lastName"));
		Assert.assertEquals(j_req.getString("age"), response.jsonPath().getString("age"));
		Assert.assertNotNull(response.jsonPath().getString("age"));

	}

}
