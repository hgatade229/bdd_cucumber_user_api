package StepDefinations;

import java.io.File;
import java.io.IOException;

import Usual_Method.Api_Trigger;
import Usual_Method.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import junit.framework.Assert;
import repository.RequestBody;

public class Get_All_User {

	
	String requestBody;

	Response response;

	int statuscode;
	
	@Given("Configure Get API for All Users")
	public void configure_get_api() throws IOException {

		File dir_name = Utility.CreateLogDir("Api_Logs");

		String endpoint = RequestBody.Hostname() + RequestBody.GetAllUsersResource();

		response = Api_Trigger.GetUserApi(RequestBody.Headername(), RequestBody.Headervalue(),
				endpoint);

		Utility.GetUsersEvidanceFile("getApi", dir_name,
				response.getBody().asPrettyString());

	}

	@When("Send the Get request with required Endpoint")
	public void send_get_request_to_the_endpoint() {

		statuscode = response.statusCode();

		System.out.println(statuscode);


	}

	@Then("validate status code for Get API")
	public void validate_status_code() {

		Assert.assertEquals(response.statusCode(), 200);

	}

}
