package StepDefinations;

import java.io.File;
import java.io.IOException;

import Usual_Method.Api_Trigger;
import Usual_Method.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import junit.framework.Assert;
import repository.RequestBody;

public class Delete_User {

	
	String requestBody;

	Response response;

	int statuscode;
	
	@Given("Configure Delete API")
	public void configure_delete_api_in_request_body() throws IOException {

		File dir_name = Utility.CreateLogDir("Api_Logs");

		String endpoint = RequestBody.Hostname() + RequestBody.DeleteUserResource("1");

		response = Api_Trigger.DeleteUserApi(RequestBody.Headername(), RequestBody.Headervalue(),
				endpoint);

		Utility.DeleteUsersEvidanceFile("deleteApi", dir_name, endpoint,
				response.getBody().asString());

	}

	@When("Send the Delete request with required Endpoint")
	public void send_delete_request_to_the_endpoint() {

		statuscode = response.statusCode();

		System.out.println(statuscode);


	}

	@Then("validate status code for Delete API")
	public void validate_status_code() {

		Assert.assertEquals(response.statusCode(), 200);

	}

}
