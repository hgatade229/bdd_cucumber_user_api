package StepDefinations;

import java.io.File;
import java.io.IOException;

import Usual_Method.Api_Trigger;
import Usual_Method.Utility;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import junit.framework.Assert;
import repository.RequestBody;

public class Update_User {

	
	String requestBody;

	Response response;

	int statuscode;

	@Given("lastName  in request body for Update API")
	public void last_name_update_in_request_body() throws IOException {

		File dir_name = Utility.CreateLogDir("Api_Logs");

		requestBody = RequestBody.updateBody("update_tc1");

		String endpoint = RequestBody.Hostname() + RequestBody.UpdateUserResource();

		response = Api_Trigger.UpdateUserApi(RequestBody.Headername(), RequestBody.Headervalue(), requestBody,
				endpoint);

		Utility.CreateEvidanceFile("updateApi", dir_name, endpoint, requestBody, response.getHeader("Date"),
				response.getBody().asString());

	}

	@When("Send the User_Update request with payload to the Endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {

		statuscode = response.statusCode();

		String responsebody = response.asPrettyString();

		System.out.println(statuscode);

		//System.out.println(responsebody);

	}

	@Then("validate status code for Update API")
	public void validate_status_code() {

		Assert.assertEquals(response.statusCode(), 200);

	}

	@Then("validate response body parametres for Update API")
	public void validate_response_body_parametres() {

		JsonPath j_req = new JsonPath(requestBody);

		Assert.assertEquals(j_req.getString("lastName"), response.jsonPath().getString("lastName"));
	}

}
